_MovementSpeed = 0.25

--  NONE = 0,
--    PLAYER = 1,
--    CHARACTER = 2,
--    LOCKED = 3

_Parent = Parent.Locked


function CheckPlayerInputs()
    Running = false

    -- MOVE UP DOWN
    if (Engine.CheckKey(Input.Z) == true) then 
        Engine.MovePlayerForward(_MovementSpeed * 10)
        Running = true
    elseif (Engine.CheckKey(Input.S) == true) then 
        Engine.MovePlayerBackward(_MovementSpeed * 10)
        Running = true
    end

    -- MOVE LEFT RIGHT
     if (Engine.CheckKey(Input.Q) == true) then 
        Engine.MovePlayerLeft(_MovementSpeed * 10)
        Running = true
    elseif (Engine.CheckKey(Input.D) == true) then 
        Engine.MovePlayerRight(_MovementSpeed * 10)
        Running = true
    end

    -- ROTATION
    if (Engine.CheckMouse(MouseAction.MoveRight) == true) then
        Engine.RotatePlayerRight(_MovementSpeed * 140)
    elseif (Engine.CheckMouse(MouseAction.MoveLeft) == true) then
        Engine.RotatePlayerLeft(_MovementSpeed * 140)
    end

    -- EXIT MENU
    if (Engine.CheckKey(Input.Escape) == true) then 
        Engine.ShowPauseMenu()
    end

    -- ANIMATE PLAYER
    if (Running == true) then
        Engine.AnimPlayer("Run")
    else
        Engine.AnimPlayer("Idle")
    end
end

-- IF FREE == TRUE CHECK MOVEMENT AND MOVE CAM
function CheckFreeInputs()
     -- MOVE FORWARD BACKARD
    if (Engine.CheckKey(Input.Z) == true)
        then Engine.MoveCameraForward(_MovementSpeed)
    elseif (Engine.CheckKey(Input.S) == true)
        then Engine.MoveCameraBackward(_MovementSpeed)
    end

    -- MOVE LEFT RIGHT
     if (Engine.CheckKey(Input.Q) == true)
        then Engine.MoveCameraLeft(_MovementSpeed)
    elseif (Engine.CheckKey(Input.D) == true)
        then Engine.MoveCameraRight(_MovementSpeed)
    end


    -- MOVE UP DOWN
     if (Engine.CheckKey(Input.UpArrow) == true)
        then Engine.MoveCameraUp(_MovementSpeed)
    elseif (Engine.CheckKey(Input.DownArrow) == true)
        then Engine.MoveCameraDown(_MovementSpeed)
    end

    -- ROTATE RIGHT LEFT
    if (Engine.CheckKey(Input.LeftArrow) == true)
        then Engine.RotateCameraLeft(_MovementSpeed * 60)
    elseif (Engine.CheckKey(Input.RightArrow) == true)
        then Engine.RotateCameraRight(_MovementSpeed * 60)
    end
end

-- FUNCTION AUTOMATICALLY CALLED FROM THE CORE ENGINE EVERY SINGLE FRAMES
function Update()
    --Print("Update From Camera")
    if (_Parent == Parent.None) 
        then CheckFreeInputs()
    elseif (_Parent == Parent.Player)
        then CheckPlayerInputs()
    end

end

function OnCharacterCreationEnd()
    _Parent = Parent.Player;
end

function OnCharacterCreationBegin()
    _Parent = Parent.None;
end


